package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import android.os.SystemClock;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by oriol.catasus on 11/01/2017.
 */
public class World {

    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SQUARE_SIZE = 32;
    static final float TICK = 0.16f;

    float tickTime = 0;
    //boolean map[][] = new int[WORLD_WIDTH][WORLD_HEIGHT];
    public ArrayList<PiecePart> partsList = new ArrayList<>();
    public Piece currentPiece;
    Piece.Type nextPieceType;
    int score = 0;
    int speed = 4;
    Random random = new Random();

    public World() {
        do {
            nextPieceType = Piece.Type.values()[random.nextInt(Piece.Type.values().length)];
        } while (nextPieceType == Piece.Type.S || nextPieceType == Piece.Type.Z);
        newPiece();
    }

    //Create a new piece
    void newPiece() {
        currentPiece = new Piece(nextPieceType);
        Piece.Type NewPiece = Piece.Type.values()[random.nextInt(Piece.Type.values().length)];
        while (NewPiece == nextPieceType) {
            NewPiece = Piece.Type.values()[random.nextInt(Piece.Type.values().length)];
        }
        nextPieceType = NewPiece;
        if (checkGameOver()) {
            GameScreen.startGameOver = SystemClock.elapsedRealtime();
            GameScreen.state = GameScreen.GameState.GameOver;
        }
        deleteLines();
    }

    public void update(float deltaTime) {
        tickTime += deltaTime;
        while(tickTime > TICK) {
            tickTime -= TICK;
            movePieceDown();
        }
    }

    boolean checkGameOver() {
        for (int i = 0; i < partsList.size(); i++) {
            if (partsList.get(i).y == 0) {
                return true;
            }
        }
        return false;
    }


    ArrayList<Integer> checkLines() {
        ArrayList<Integer> lines = new ArrayList<>();
        for (int y = WORLD_HEIGHT; y >= 0; y--) {
            int numPieces = 0;
            for (int i = 0; i < partsList.size(); i++) {
                if (partsList.get(i).y == y * SQUARE_SIZE - SQUARE_SIZE) {
                    numPieces++;
                }
            }
            if (numPieces == WORLD_WIDTH) {
                lines.add(y);
            } else if (numPieces == 0) {
                break;
            }
        }
        return lines;
    }

    void deleteLines() {
        ArrayList<Integer> linesToDelete = checkLines();
        while (linesToDelete.size() > 0) {
            for (int j = 0; j < linesToDelete.size(); j++) {
                for (int i = 0; i < partsList.size(); i++) {
                    if (partsList.get(i).y == linesToDelete.get(j) * SQUARE_SIZE - SQUARE_SIZE) {
                        partsList.get(i).y = WORLD_HEIGHT * SQUARE_SIZE * 2;
                        partsList.remove(i);
                        i--;
                    }
                }
                moveLinesDown(linesToDelete.get(j) * SQUARE_SIZE - SQUARE_SIZE);
                score += speed;
                speed++;
            }
            linesToDelete = checkLines();
        }
    }

    void moveLinesDown(int y) {
        for (int i = 0; i < partsList.size(); i++) {
            if (partsList.get(i).y < y) {
                partsList.get(i).y += SQUARE_SIZE;
            }
        }
    }

    void movePieceDown() {
        if (canMoveDown()) {
            for (int i = 0; i < currentPiece.parts.length; i++) {
                currentPiece.parts[i].y += speed + currentPiece.parts[i].speed;
            }
        } else {
            transferPieceToWorld();
            newPiece();
        }
    }
    //Use this to check if the piece can move down
    boolean canMoveDown() {
        for (int i = 0; i < currentPiece.parts.length; i++) {
            for (int a = 0; a < partsList.size(); a++) {    //If I put this loop after the next if, collision of the piece sometimes doesn't work good.
                if (partsList.get(a).x == currentPiece.parts[i].x && currentPiece.parts[i].y < partsList.get(a).y && currentPiece.parts[i].y + speed + currentPiece.parts[i].speed >= partsList.get(a).y - SQUARE_SIZE) {
                    correctionCol(partsList.get(a).y - SQUARE_SIZE - currentPiece.parts[i].y);
                    return false;
                }
            }
            if (currentPiece.parts[i].y + speed + currentPiece.parts[i].speed >= WORLD_HEIGHT * SQUARE_SIZE - SQUARE_SIZE) {
                correctionCol((WORLD_HEIGHT * SQUARE_SIZE - SQUARE_SIZE) - currentPiece.parts[i].y);
                return false;
            }
        }
        return true;
    }

    public boolean colisionOnRotate() {
        for (int i = 0; i < currentPiece.parts.length; i++) {
            if (currentPiece.parts[i].x >= WORLD_WIDTH * SQUARE_SIZE || currentPiece.parts[i].y >= WORLD_HEIGHT * SQUARE_SIZE) {
                return true;
            } else {
                for (int a = 0; a < partsList.size(); a++) {
                    if (partsList.get(a).x == currentPiece.parts[i].x && currentPiece.parts[i].y < partsList.get(a).y && currentPiece.parts[i].y >= partsList.get(a).y - SQUARE_SIZE) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //Use this for acurate collision
    void correctionCol(int correction) {
        for (int a = 0; a < currentPiece.parts.length; a++) {
            currentPiece.parts[a].y += correction;
        }
    }

    //Use this to transfer parts from the current piece to  partsList
    void transferPieceToWorld() {
        for (int i = 0; i < currentPiece.parts.length; i++) {
            partsList.add(currentPiece.parts[i]);
        }
    }

    //This methods move the piece in a dir
    public void goRight() {
        if (currentPiece.checkHighSpeed()) {
            return;
        }
        if (checkRightSide()) {
            for(int i = 0; i < currentPiece.parts.length; i++) {
                currentPiece.parts[i].x += SQUARE_SIZE;
            }
        }
    }
    public void goLeft() {
        if (currentPiece.checkHighSpeed()) {
            return;
        }
        if (checkLeftSide()) {
            for(int i = 0; i < currentPiece.parts.length; i++) {
                currentPiece.parts[i].x -= SQUARE_SIZE;
            }
        }

    }

    boolean checkRightSide() {
        for (int i = 0; i < currentPiece.parts.length; i++) {
            if (currentPiece.parts[i].x + SQUARE_SIZE > WORLD_WIDTH * SQUARE_SIZE - SQUARE_SIZE) {
                return false;
            } else {
                for (int a = 0; a < partsList.size(); a++) {
                    if ((partsList.get(a).x == currentPiece.parts[i].x + SQUARE_SIZE) && (partsList.get(a).y < currentPiece.parts[i].y + SQUARE_SIZE * 4 / 5 && partsList.get(a).y + SQUARE_SIZE / 2 > currentPiece.parts[i].y)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    boolean checkLeftSide() {
        for (int i = 0; i < currentPiece.parts.length; i++) {
            if (currentPiece.parts[i].x == 0) {
                return false;
            } else {
                for (int a = 0; a < partsList.size(); a++) {
                    if ((partsList.get(a).x == currentPiece.parts[i].x - SQUARE_SIZE) && (partsList.get(a).y < currentPiece.parts[i].y + SQUARE_SIZE * 4 / 5 && partsList.get(a).y + SQUARE_SIZE / 2 > currentPiece.parts[i].y)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void goDown() {
        for (int i = 0; i < currentPiece.parts.length; i++) {
            currentPiece.parts[i].speed = currentPiece.maxSpeed;
        }
    }
}