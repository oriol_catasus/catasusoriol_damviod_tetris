package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import android.graphics.Color;
import android.os.SystemClock;

import java.util.ArrayList;
import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by oriol.catasus on 11/01/2017.
 */
public class GameScreen extends Screen {

    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver
    }
    public static GameState state;

    World world;
    String score = "0";

    //Slice Touch
    int[] prevTouchPos = new int[2];
    int minLenghtTouch = 5;

    //GameOver
    int timeWait = 2 * 1000;
    public static float startGameOver;

    public GameScreen(Game game) {
        super(game);
        state = GameState.Ready;
        world = new World();
    }

    @Override
    public void update(float deltaTime)
    {
        List<Input.TouchEvent> listTouchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        switch (state) {
            case Ready:
                updateReady(listTouchEvents);
                break;
            case Running:
                updateRunning(listTouchEvents, deltaTime);
                break;
            case Paused:
                updatePaused(listTouchEvents);
                break;
            case GameOver:
                updateGameOver(listTouchEvents);
                break;
        }
    }

    void updateReady(List<Input.TouchEvent> listTouchEvents) {
        if (listTouchEvents.size() > 0 && listTouchEvents.get(0).type == listTouchEvents.get(0).TOUCH_DOWN) {
            Assets.click.play(1);
            state = GameState.Running;
        }
    }

    void updateGameOver(List<Input.TouchEvent> listTouchEvents) {
        float actualTime = SystemClock.elapsedRealtime();
        if (listTouchEvents.size() > 0 && actualTime > timeWait + startGameOver) {
            Assets.click.play(1);
            game.setScreen(new MainMenu(game));
        }
    }

    void updateRunning(List<Input.TouchEvent> listTouchEvents, float deltaTime) {
        for (int i = 0; i < listTouchEvents.size(); i++) {
            Input.TouchEvent touchEvent = listTouchEvents.get(i);
            if (touchEvent.type == Input.TouchEvent.TOUCH_UP && prevTouchPos[0] != 0 && prevTouchPos[1] != 0) {
                if (Math.abs(touchEvent.x - prevTouchPos[0]) > minLenghtTouch || Math.abs(touchEvent.y - prevTouchPos[1]) > minLenghtTouch) {
                    if (Math.abs(touchEvent.x - prevTouchPos[0]) > Math.abs(touchEvent.y - prevTouchPos[1])) {
                        if (touchEvent.x > prevTouchPos[0]) {
                            world.goRight();
                        } else {
                            world.goLeft();
                        }
                    } else if (touchEvent.y > prevTouchPos[1]) {
                        world.goDown();
                    }
                } else if(touchEvent.x < 64 && touchEvent.y < 64) {
                    Assets.click.play(1);
                    state = GameState.Paused;
                    return;
                } else {
                    world.currentPiece.rotate();
                    if (world.colisionOnRotate()) {
                        world.currentPiece.revertRotation();
                    }
                }
            } else if (touchEvent.type == Input.TouchEvent.TOUCH_DOWN) {
                prevTouchPos[0] = touchEvent.x;
                prevTouchPos[1] = touchEvent.y;
            }
        }
        world.update(deltaTime);
    }

    void updatePaused(List<Input.TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if(event.type == Input.TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        Assets.click.play(1);
                        state = GameState.Running;
                        return;
                    }
                    if(event.y > 148 && event.y < 196) {
                        Assets.click.play(1);
                        game.setScreen(new MainMenu(game));
                        return;
                    }
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        drawWorld(g);
        switch (state) {
            case Ready:
                g.drawPixmap(Assets.Ready, 47, 100);
                break;
            case Paused:
                g.drawPixmap(Assets.PauseText, 80, 100);
                break;
            case GameOver:
                g.drawPixmap(Assets.GameOver, 47, 100);
                break;
        }
        drawWorldUI(g);
    }

    void drawWorld(Graphics g) {
        g.drawPixmap(Assets.MenuBackground, 0, 0);
        for (int i = 0; i < world.currentPiece.parts.length; i++) {
            g.drawPixmap(world.currentPiece.parts[i].graphic, world.currentPiece.parts[i].x, world.currentPiece.parts[i].y);
        }
        for (int i = 0; i < world.partsList.size(); i++) {
            g.drawPixmap(world.partsList.get(i).graphic, world.partsList.get(i).x, world.partsList.get(i).y);
        }
    }

    void drawWorldUI(Graphics g) {
        g.drawLine(0, 416, 480, 416, Color.WHITE);
        g.drawPixmap(Assets.PauseButton, 0, 0);
        score = ""+world.score;
        drawText(g, score, g.getWidth() / 5 - score.length()*20 / 2, g.getHeight() - 42);
        drawNextPiece(g);
    }

    void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }

            g.drawPixmap(Assets.Numbers, x, y, srcX, 0, srcWidth, 32);
            x += srcWidth;
        }
    }

    void drawNextPiece(Graphics g) {
        int posX = g.getWidth() * 3 / 5;
        int posY = g.getHeight() - 50;
        switch (world.nextPieceType) {
            case O:
                g.drawPixmap(Assets.NextShape[0], posX, posY);
                break;
            case I:
                g.drawPixmap(Assets.NextShape[1], posX, posY);
                break;
            case S:
                g.drawPixmap(Assets.NextShape[2], posX, posY);
                break;
            case L:
                g.drawPixmap(Assets.NextShape[3], posX, posY);
                break;
            case T:
                g.drawPixmap(Assets.NextShape[4], posX, posY);
                break;
            case Z:
                g.drawPixmap(Assets.NextShape[5], posX, posY);
                break;
            case J:
                g.drawPixmap(Assets.NextShape[6], posX, posY);
                break;
        }
    }

    @Override
    public void pause() {
        if(state == GameState.Running)
            state = GameState.Paused;
        Assets.music.pause();
    }

    @Override
    public void resume() {
        Assets.music.play();
    }

    @Override
    public void dispose() {

    }
}
