package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by oriol.catasus on 11/01/2017.
 */
public class Button {
    public int x;
    public int y;
    public Pixmap texture;
    public Screen screen;

    public Button(int _x, int _y, Pixmap _texture, Screen _screen) {
        x = _x;
        y = _y;
        texture = _texture;
        screen = _screen;
    }
}
