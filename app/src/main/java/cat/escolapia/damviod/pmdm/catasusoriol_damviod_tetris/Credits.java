package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by oriol.catasus on 11/01/2017.
 */
public class Credits extends Screen {
    public Credits(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        List<Input.TouchEvent> listTouchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if (listTouchEvents.size() > 0) {
            for (int i = 0; i < listTouchEvents.size(); i++) {
                if (listTouchEvents.get(i).type == listTouchEvents.get(i).TOUCH_DOWN){
                    Assets.click.play(1);
                    game.setScreen(new MainMenu(game));
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.MenuBackground, 0, 0);
        g.drawPixmap(Assets.Credits, 64, 64);
    }

    @Override
    public void pause() {
        Assets.music.pause();
    }

    @Override
    public void resume() {
        Assets.music.play();
    }

    @Override
    public void dispose() {

    }
}
