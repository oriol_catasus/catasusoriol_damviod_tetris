package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import cat.escolapia.damviod.pmdm.framework.Pixmap;

/**
 * Created by oriol.catasus on 11/01/2017.
 */
public class PiecePart {
    public int x;
    public int y;
    public int speed;
    public Pixmap graphic;

    public PiecePart(int _x, int _y, Pixmap _graphic){
        x = _x;
        y = _y;
        graphic = _graphic;
    }
}
