package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.impl.AndroidGame;

/**
 * Created by oriol.catasus on 21/12/2016.
 */
public class Tetris extends AndroidGame {
    public Screen getStartScreen() {
        return new LoadingScreen(this);
    }
}
