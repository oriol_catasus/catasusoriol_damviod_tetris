package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import cat.escolapia.damviod.pmdm.framework.Music;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

/**
 * Created by oriol.catasus on 21/12/2016.
 */
public class Assets {

    //MainMenu Assets
    public static Pixmap Logo;
    public static Pixmap MenuBackground;
    public static Pixmap PlayButton;
    public static Pixmap HighscoreButton;
    public static Pixmap CreditsButton;

    //Pieces Assets
    public static Pixmap[] Pieces = new Pixmap[7];

    //UI
    public static Pixmap Ready;
    public static Pixmap Numbers;
    public static Pixmap NextShape[] = new Pixmap[7];
    public static Pixmap GameOver;

    //Pause
    public static Pixmap PauseButton;
    public static Pixmap PauseText;

    //Credits
    public static Pixmap Credits;

    //Music
    public static Music music;
    public static Sound click;

    public static int centerPointX(int widthAsset) {
        return 160 - (widthAsset / 2);
    }

    public static int centerPointUpY(int heightAsset) {
        return 120 - (heightAsset / 2);
    }
}
