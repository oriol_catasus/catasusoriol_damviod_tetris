package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import java.util.Random;

import cat.escolapia.damviod.pmdm.framework.Pixmap;

/**
 * Created by oriol.catasus on 11/01/2017.
 */
public class Piece {

    public enum Type {
        O, I, S, L, T, Z, J
    }
    Random rnd = new Random();
    public Type type;
    boolean[][] shape;
    public PiecePart[] parts = new PiecePart[4];
    int rotation = 0;
    public int maxSpeed = 16;
    public Pixmap colorPart;

    //For revert rotates
    int prevRotation;
    boolean[][] preRotationShape;
    PiecePart[] preRotationParts = new PiecePart[4];


    public Piece(Type _type) {
        colorPart = Assets.Pieces[rnd.nextInt(Assets.Pieces.length)];
        type = _type;
        setShape();
    }

    void setShape() {
        switch (type) {
            case I:
                shapeI();
                break;
            case L:
                shapeL();
                break;
            case O:
                shapeO();
                break;
            case S:
                shapeS();
                break;
            case T:
                shapeT();
                break;
            case J:
                shapeJ();
                break;
            case Z:
                shapeZ();
                break;
        }
        int xPos = rnd.nextInt(World.WORLD_WIDTH);
        if (xPos > World.WORLD_WIDTH - shape.length) {
            xPos = World.WORLD_WIDTH - shape.length;
        }
        xPos = xPos * World.SQUARE_SIZE;
        setPieces(xPos, -World.SQUARE_SIZE);
    }

    //Use this method to instance the parts of the piece with his corresponding shape
    void setPieces(int xPos, int yPos) {
        int i = 0;
        for (int x = 0; x < shape.length; x++) {
            for (int y = 0; y < shape[x].length; y++) {
                if (shape[x][y]) {
                    PiecePart NewPiece = new PiecePart(xPos + (x * World.SQUARE_SIZE), yPos - (y * World.SQUARE_SIZE), colorPart);
                    parts[i] = NewPiece;
                    i++;
                }
            }
        }
    }

    void shapeI() {
        shape = new boolean[1][4];
        shape[0][0] = true;
        shape[0][1] = true;
        shape[0][2] = true;
        shape[0][3] = true;
    }
    void shapeJ() {
        shape = new boolean[3][2];
        shape[2][0] = true;
        shape[0][1] = true;
        shape[1][1] = true;
        shape[2][1] = true;
    }
    void shapeL() {
        shape = new boolean[3][2];
        shape[0][0] = true;
        shape[0][1] = true;
        shape[1][1] = true;
        shape[2][1] = true;
    }
    void shapeO() {
        shape = new boolean[2][2];
        shape[0][0] = true;
        shape[0][1] = true;
        shape[1][0] = true;
        shape[1][1] = true;
    }
    void shapeS() {
        shape = new boolean[3][2];
        shape[0][0] = true;
        shape[1][0] = true;
        shape[1][1] = true;
        shape[2][1] = true;
    }
    void shapeZ() {
        shape = new boolean[3][2];
        shape[0][1] = true;
        shape[1][0] = true;
        shape[1][1] = true;
        shape[2][0] = true;
    }
    void shapeT() {
        shape = new boolean[3][2];
        shape[0][0] = true;
        shape[1][0] = true;
        shape[1][1] = true;
        shape[2][0] = true;
    }

    public void rotate() {
        if (checkHighSpeed() || type == Type.O) {
            return;
        }
        savePreviousRotation();
        switch (type) {
            case I:
                newShapeI();
                break;
            case T:
                newShapeT();
                break;
            case S:
                newShapeS();
                break;
            case L:
                newShapeL();
                break;
            case J:
                newShapeJ();
                break;
            case Z:
                newShapeZ();
        }
    }

    void savePreviousRotation() {
        prevRotation = rotation;
        preRotationShape = shape;
        for (int i = 0; i < parts.length; i++) {
            PiecePart NewBlock = new PiecePart(parts[i].x, parts[i].y, parts[i].graphic);
            preRotationParts[i] = NewBlock;
        }
    }

    //Use this to revert a rotation
    public void revertRotation() {
        rotation = prevRotation;
        shape = preRotationShape;
        parts = preRotationParts;
        prevRotation = 0;
        preRotationShape = null;
        preRotationParts = new PiecePart[4];
    }

    boolean checkHighSpeed() {
        if (parts[0].speed == maxSpeed) {
            return true;
        }
        return false;
    }

    void newShapeI() {
        switch(rotation) {
            case 0:
                boolean[][] newShape = new boolean[4][1];
                newShape[0][0] = true;
                newShape[1][0] = true;
                newShape[2][0] = true;
                newShape[3][0] = true;
                shape = newShape;
                setPieces(parts[1].x - World.SQUARE_SIZE, parts[1].y);
                rotation++;
                break;
            case 1:
                shapeI();
                setPieces(parts[1].x, parts[1].y + World.SQUARE_SIZE);
                rotation = 0;
        }
    }

    void newShapeT() {
        boolean[][] newShape;
        switch (rotation) {
            case 0:
                newShape = new boolean[2][3];
                newShape[0][0] = true;
                newShape[0][1] = true;
                newShape[0][2] = true;
                newShape[1][1] = true;
                shape = newShape;
                setPieces(parts[2].x - World.SQUARE_SIZE, parts[2].y + World.SQUARE_SIZE);
                rotation++;
                break;
            case 1:
                newShape = new boolean[3][2];
                newShape[0][1] = true;
                newShape[1][1] = true;
                newShape[1][0] = true;
                newShape[2][1] = true;
                shape = newShape;
                setPieces(parts[1].x, parts[1].y);
                rotation++;
                break;
            case 2:
                newShape = new boolean[2][3];
                newShape[1][0] = true;
                newShape[1][1] = true;
                newShape[0][1] = true;
                newShape[1][2] = true;
                shape = newShape;
                setPieces(parts[1].x, parts[1].y + World.SQUARE_SIZE);
                rotation++;
                break;
            case 3:
                shapeT();
                setPieces(parts[1].x - World.SQUARE_SIZE * 2, parts[1].y);
                rotation = 0;
        }
    }

    void newShapeS() {
        boolean[][] newShape;
        switch (rotation) {
            case 0:
                newShape = new boolean[2][3];
                newShape[0][1] = true;
                newShape[0][2] = true;
                newShape[1][0] = true;
                newShape[1][1] = true;
                shape = newShape;
                setPieces(parts[3].x - World.SQUARE_SIZE, parts[3].y + World.SQUARE_SIZE);
                rotation++;
                break;
            case 1:
                shapeS();
                setPieces(parts[2].x - World.SQUARE_SIZE * 2, parts[2].y);
                rotation = 0;
                break;
        }
    }

    void newShapeZ() {
        boolean[][] newShape;
        switch (rotation) {
            case 0:
                newShape = new boolean[2][3];
                newShape[0][0] = true;
                newShape[0][1] = true;
                newShape[1][1] = true;
                newShape[1][2] = true;
                shape = newShape;
                setPieces(parts[3].x - World.SQUARE_SIZE, parts[3].y + World.SQUARE_SIZE);
                rotation++;
                break;
            case 1:
                shapeS();
                setPieces(parts[2].x - World.SQUARE_SIZE * 2, parts[2].y + World.SQUARE_SIZE);
                rotation = 0;
        }
    }

    void newShapeL() {
        boolean[][] newShape;
        switch (rotation) {
            case 0:
                newShape = new boolean[2][3];
                newShape[0][0] = true;
                newShape[0][1] = true;
                newShape[0][2] = true;
                newShape[1][0] = true;
                shape = newShape;
                setPieces(parts[2].x, parts[2].y + World.SQUARE_SIZE);
                rotation++;
                break;
            case 1:
                newShape = new boolean[3][2];
                newShape[0][0] = true;
                newShape[1][0] = true;
                newShape[2][0] = true;
                newShape[2][1] = true;
                shape = newShape;
                setPieces(parts[2].x - World.SQUARE_SIZE, parts[2].y + World.SQUARE_SIZE);
                rotation++;
                break;
            case 2:
                newShape = new boolean[2][3];
                newShape[0][2] = true;
                newShape[1][0] = true;
                newShape[1][1] = true;
                newShape[1][2] = true;
                shape = newShape;
                setPieces(parts[2].x - World.SQUARE_SIZE * 2, parts[2].y + World.SQUARE_SIZE * 2);
                rotation++;
                break;
            case 3:
                shapeL();
                setPieces(parts[1].x - World.SQUARE_SIZE, parts[1].y);
                rotation = 0;
                break;
        }
    }

    void newShapeJ() {
        boolean[][] newShape;
        switch (rotation) {
            case 0:
                newShape = new boolean[2][3];
                newShape[0][0] = true;
                newShape[0][1] = true;
                newShape[0][2] = true;
                newShape[1][2] = true;
                shape = newShape;
                setPieces(parts[2].x - World.SQUARE_SIZE, parts[2].y + World.SQUARE_SIZE);
                rotation++;
                break;
            case 1:
                newShape = new boolean[3][2];
                newShape[0][0] = true;
                newShape[0][1] = true;
                newShape[1][0] = true;
                newShape[2][0] = true;
                shape = newShape;
                setPieces(parts[1].x - World.SQUARE_SIZE, parts[1].y);
                rotation++;
                break;
            case 2:
                newShape = new boolean[2][3];
                newShape[0][0] = true;
                newShape[1][0] = true;
                newShape[1][1] = true;
                newShape[1][2] = true;
                shape = newShape;
                setPieces(parts[2].x - World.SQUARE_SIZE, parts[2].y + World.SQUARE_SIZE);
                rotation++;
                break;
            case 3:
                shapeJ();
                setPieces(parts[1].x - World.SQUARE_SIZE, parts[1].y + World.SQUARE_SIZE);
                rotation = 0;
                break;
        }
    }
}
