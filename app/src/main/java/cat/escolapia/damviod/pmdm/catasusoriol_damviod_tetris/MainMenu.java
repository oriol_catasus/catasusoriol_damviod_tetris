package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by oriol.catasus on 14/12/2016.
 */
public class MainMenu extends Screen {

    Button[] buttons = new Button[2];


    public MainMenu(Game game) {
        super (game);
        initButtons();
    }

    void initButtons() {
        buttons[0] = new Button(Assets.centerPointX(Assets.PlayButton.getWidth()), 220, Assets.PlayButton, new GameScreen(game));
        buttons[1] = new Button(Assets.centerPointX(Assets.CreditsButton.getWidth()),292, Assets.CreditsButton, new Credits(game));
    }

    public void update(float deltaTime) {
        List<Input.TouchEvent> listTouchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        for (int i = 0; i < listTouchEvents.size(); i++) {
            Input.TouchEvent touchEvent = listTouchEvents.get(i);
            if (touchEvent.type == Input.TouchEvent.TOUCH_DOWN) {
                loadScreen(touchEvent);
            }
        }
    }

    void loadScreen (Input.TouchEvent event) {
        for (int i = 0; i < buttons.length; i++) {
            if (event.x > buttons[i].x && event.x < buttons[i].x + buttons[i].texture.getWidth() &&
                    event.y > buttons[i].y && event.y < buttons[i].y + buttons[i].texture.getHeight()) {
                Assets.click.play(1);
                game.setScreen(buttons[i].screen);
                return;
            }
        }
    }

    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.MenuBackground, 0, 0);
        g.drawPixmap(Assets.Logo, Assets.centerPointX(Assets.Logo.getWidth()), Assets.centerPointUpY(Assets.Logo.getHeight()));
        for (int i = 0; i < buttons.length; i++) {
            g.drawPixmap(buttons[i].texture, buttons[i].x, buttons[i].y);
        }
    }

    public void pause() {
        Assets.music.pause();
    }

    public void resume() {
        Assets.music.play();
    }

    public void dispose() {

    }
}
