package cat.escolapia.damviod.pmdm.catasusoriol_damviod_tetris;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by oriol.catasus on 21/12/2016.
 */
public class LoadingScreen extends Screen {

    public LoadingScreen(Game game) {
        super (game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();

        //Main menu Assets
        Assets.Logo = g.newPixmap("Logo.png", Graphics.PixmapFormat.ARGB4444);
        Assets.MenuBackground = g.newPixmap("Backgrounds/MenuBackground.jpg", Graphics.PixmapFormat.RGB565);
        Assets.PlayButton = g.newPixmap("Menu/PlayButton.png", Graphics.PixmapFormat.ARGB4444);
        Assets.HighscoreButton = g.newPixmap("Menu/HighscoreButton.png", Graphics.PixmapFormat.ARGB4444);
        Assets.CreditsButton = g.newPixmap("Menu/CreditsButton.png", Graphics.PixmapFormat.ARGB4444);

        //Pieces Assets
        Assets.Pieces[0] = g.newPixmap("Pieces/RedPiece.jpg", Graphics.PixmapFormat.RGB565);
        Assets.Pieces[1] = g.newPixmap("Pieces/GreenPiece.jpg", Graphics.PixmapFormat.RGB565);
        Assets.Pieces[2] = g.newPixmap("Pieces/BluePiece.jpg", Graphics.PixmapFormat.RGB565);
        Assets.Pieces[3] = g.newPixmap("Pieces/WhitePiece.jpg", Graphics.PixmapFormat.RGB565);
        Assets.Pieces[4] = g.newPixmap("Pieces/CyanPiece.jpg", Graphics.PixmapFormat.RGB565);
        Assets.Pieces[5] = g.newPixmap("Pieces/YellowPiece.jpg", Graphics.PixmapFormat.RGB565);
        Assets.Pieces[6] = g.newPixmap("Pieces/PinkPiece.jpg", Graphics.PixmapFormat.RGB565);

        //UI
        Assets.Ready = g.newPixmap("GameScreenUI/ready.png", Graphics.PixmapFormat.ARGB4444);
        Assets.Numbers = g.newPixmap("GameScreenUI/numbers.png", Graphics.PixmapFormat.ARGB4444);
        Assets.GameOver = g.newPixmap("GameScreenUI/gameover.png", Graphics.PixmapFormat.ARGB4444);

        //Pause
        Assets.PauseButton = g.newPixmap("GameScreenUI/PauseButton.png", Graphics.PixmapFormat.ARGB4444);
        Assets.PauseText = g.newPixmap("GameScreenUI/PauseText.png", Graphics.PixmapFormat.ARGB4444);

        //NextShape UI
        Assets.NextShape[0] = g.newPixmap("GameScreenUI/NextShape/ShapeO.jpg", Graphics.PixmapFormat.RGB565);
        Assets.NextShape[1] = g.newPixmap("GameScreenUI/NextShape/ShapeI.jpg", Graphics.PixmapFormat.RGB565);
        Assets.NextShape[2] = g.newPixmap("GameScreenUI/NextShape/ShapeS.png", Graphics.PixmapFormat.ARGB4444);
        Assets.NextShape[3] = g.newPixmap("GameScreenUI/NextShape/ShapeL.png", Graphics.PixmapFormat.ARGB4444);
        Assets.NextShape[4] = g.newPixmap("GameScreenUI/NextShape/ShapeT.png", Graphics.PixmapFormat.ARGB4444);
        Assets.NextShape[5] = g.newPixmap("GameScreenUI/NextShape/ShapeZ.png", Graphics.PixmapFormat.ARGB4444);
        Assets.NextShape[6] = g.newPixmap("GameScreenUI/NextShape/ShapeJ.png", Graphics.PixmapFormat.ARGB4444);

        //Credits Screen
        Assets.Credits = g.newPixmap("Name.png", Graphics.PixmapFormat.ARGB4444);

        //Music
        Assets.click = game.getAudio().newSound("Sound/click.ogg");
        Assets.music = game.getAudio().newMusic("Sound/OriginalTetrisSoundTrack.mp3");
        Assets.music.setLooping(true);  //Put the music in loop mode and play it
        Assets.music.play();

        game.setScreen(new MainMenu(game));
    }

    public void render(float deltaTime) {

    }

    public void pause() {
    }

    public void resume() {
    }

    public void dispose() {

    }
}
