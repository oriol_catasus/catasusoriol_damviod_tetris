
public class testRotate {

	public static void main(String[] args) {
		
		System.out.println("Exemple amb shape acotada");
		Piece l = new Piece();
		System.out.println("Abans de rotar....");
		drawPiece(l);		
		l.Rotate();		
		System.out.println("Despres de rotar  ....");
		drawPiece(l);		
		l.Rotate();		
		System.out.println("Despres de tornar a rotar ....");
		drawPiece(l);
		

		System.out.println("\n\n");
				
		System.out.println("Exemple amb shape fixa de 5x5");
		PieceAlternative l2 = new PieceAlternative();
		System.out.println("Abans de rotar....");
		drawPiece(l2);		
		l2.Rotate();		
		System.out.println("Despres de rotar  ....");
		drawPiece(l2);		
		l2.Rotate();		
		System.out.println("Despres de tornar a rotar ....");
		drawPiece(l2);		

	}
	
	public static void drawPiece(PieceObject piece) {
        for(int i = 0; i < piece.getShape().length; i++){
            for(int j = 0; j < piece.getShape()[0].length; j++){            
            	System.out.print(piece.getShape()[i][j] ? "*" : "�");
            }
            System.out.println(" ");
        }

	}

}
