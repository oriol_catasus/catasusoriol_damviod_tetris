
public class Piece implements PieceObject{
	
	public int x; // X Position in the board
    public int y; // Y Position in the board
	public boolean[][] shape; 
	public String color;
	
	public Piece() {
		shape = new boolean[3][2];        
        shape[0][1] = true;
        shape[1][1] = true;
        shape[2][1] = true;
        shape[2][0] = true;
        color = "1";
	}

    public void Rotate(){
        final int arrayDimension1 = shape.length;
        final int arrayDimension2 = shape[0].length;
        boolean[][] newShape = new boolean[arrayDimension2][arrayDimension1];

        for (int i = 0; i < arrayDimension1; i++) {
            for (int j = 0; j < arrayDimension2; j++) {
                newShape[arrayDimension2-j-1][i] = shape[i][j];
            }
        }

        shape = newShape;
    }

    public boolean[][] getShape() {
    	return shape;
    }
	
	
}
