
public class PieceAlternative implements PieceObject {
	public int x; // X Position in the board
    public int y; // Y Position in the board
	public boolean[][] shape = new boolean[5][5]; 
	public String color;
	
	public PieceAlternative() {        
        shape[1][2] = true;
        shape[2][2] = true;
        shape[3][2] = true;
        shape[3][1] = true;
        color = "1";
	}

    public void Rotate(){
        boolean[][] newShape = new boolean[5][5];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                newShape[5-j-1][i] = shape[i][j];
            }
        }
        shape = newShape;
    }
	
    public boolean[][] getShape() {
    	return shape;
    }
	
}
